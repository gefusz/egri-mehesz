<?php
/**
 * A WordPress fő konfigurációs állománya
 *
 * Ebben a fájlban a következő beállításokat lehet megtenni: MySQL beállítások
 * tábla előtagok, titkos kulcsok, a WordPress nyelve, és ABSPATH.
 * További információ a fájl lehetséges opcióiról angolul itt található:
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 *  A MySQL beállításokat a szolgáltatónktól kell kérni.
 *
 * Ebből a fájlból készül el a telepítési folyamat közben a wp-config.php
 * állomány. Nem kötelező a webes telepítés használata, elegendő átnevezni
 * "wp-config.php" névre, és kitölteni az értékeket.
 *
 * @package WordPress
 */

// ** MySQL beállítások - Ezeket a szolgálatótól lehet beszerezni ** //
/** Adatbázis neve */
define( 'DB_NAME', 'egrimeheszbolt' );

/** MySQL felhasználónév */
define( 'DB_USER', 'root' );

/** MySQL jelszó. */
define( 'DB_PASSWORD', '' );

/** MySQL  kiszolgáló neve */
define( 'DB_HOST', 'localhost' );

/** Az adatbázis karakter kódolása */
define( 'DB_CHARSET', 'utf8mb4' );

/** Az adatbázis egybevetése */
define('DB_COLLATE', '');

/**#@+
 * Bejelentkezést tikosító kulcsok
 *
 * Változtassuk meg a lenti konstansok értékét egy-egy tetszóleges mondatra.
 * Generálhatunk is ilyen kulcsokat a {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org titkos kulcs szolgáltatásával}
 * Ezeknek a kulcsoknak a módosításával bármikor kiléptethető az összes bejelentkezett felhasználó az oldalról.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'kkeegEBG?gDwKkG^7^k3P`$YvT$m;:D,wDr+xwrSp!^_12&>N;[s5RV?fgSGl;W>' );
define( 'SECURE_AUTH_KEY', '6sgoFgjfwE}71Bcv)k:lHE}w(7-i2]ph{Ag~_JWCTdP#,fdERc{4%$6llTi^Vl])' );
define( 'LOGGED_IN_KEY', '(#pW!a#@{:yI7dxsA>)OprO(S*Ta=(<l>S264I/nV!ui;7-Q+9X]G]g&JSVAciTp' );
define( 'NONCE_KEY', '%)OcUiZSiG$%Nzx!#FjmM&f3j_PA[aG |1D!6GK2!nJ>x6LG&86LXy7RM:qX[Vo2' );
define( 'AUTH_SALT',        'ECx{zN,(iJE~>+CL6Br_{zlC1G4vq>J#]lf~F`5:atZA<F?qmLlx` PZzhbF iz/' );
define( 'SECURE_AUTH_SALT', 'bLdIzjuYWt{(tA&XH+Wc(qB]7`u2>]kRtU)4j&u?00rO[)RYm)m)Z#x^z)8@VIjV' );
define( 'LOGGED_IN_SALT',   'y.g^xx%9g7/7Rv&y[tEaU,Gn%X+ew|@~f^8x>V5}2k,N@=A:uE)w2X l jvG~Tm!' );
define( 'NONCE_SALT',       '$%@GL9;vP(sLhKq-n[rh;bFlKcG,f]SZnd DH?W^{pM1o>Sxc4zPa-d#,hF{#}ZZ' );

/**#@-*/

/**
 * WordPress-adatbázis tábla előtag.
 *
 * Több blogot is telepíthetünk egy adatbázisba, ha valamennyinek egyedi
 * előtagot adunk. Csak számokat, betűket és alulvonásokat adhatunk meg.
 */
$table_prefix = 'wp_';

/**
 * Fejlesztőknek: WordPress hibakereső mód.
 *
 * Engedélyezzük ezt a megjegyzések megjelenítéséhez a fejlesztés során.
 * Erősen ajánlott, hogy a bővítmény- és sablonfejlesztők használják a WP_DEBUG
 * konstansot.
 */
define('WP_DEBUG', false);

/* Ennyi volt, kellemes blogolást! */

/** A WordPress könyvtár abszolút elérési útja. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Betöltjük a WordPress változókat és szükséges fájlokat. */
require_once(ABSPATH . 'wp-settings.php');
