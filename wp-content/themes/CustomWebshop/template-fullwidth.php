<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Full width
 *
 * @package storefront
 */

get_header(); ?>
	</div><!-- .col-full -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section id="hero">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 category-list">
							<ul>
							<?php 
							$terms = get_terms( 'product_cat' ); 
							foreach( $terms as $term ) 
							{ ?>
							<li><a href="<?php echo site_url(); ?>/termekkategoria/<?php echo $term->slug; ?>"><?php echo $term->name;?></a>(<?php echo $term->count; ?>)</li>
							<?php } ?>
							 </ul>
						</div>
						<div class="col-lg-8 featured-product">
							
						</div>
					</div>
				</div>
			</section>
			<section id="excellence">
				<div class="container">
					<div class="row">
						<div class="col-lg-4">
							<img src="<?php echo woocommerce_placeholder_img_src(); ?>">
							<p>Lipsum generator: Lorem Ipsum - All the facts</p>
						</div>
						<div class="col-lg-4">
							<img src="<?php echo woocommerce_placeholder_img_src(); ?>">
							<p>Lipsum generator: Lorem Ipsum - All the facts</p>
						</div>
						<div class="col-lg-4">
							<img src="<?php echo woocommerce_placeholder_img_src(); ?>">
							<p>Lipsum generator: Lorem Ipsum - All the facts</p>
						</div>
					</div>
				</div>
			</section>
			<section id="new-products">
				<div class="container">
					<h2 class="section-title">Új termékek</h2>
					<div class="container">
						<div class="row products">
							<?php
								$args_new = array(
									'post_type' => 'product',
									'posts_per_page' => 4,
							    	'orderby' => 'date',
	   								'order' => 'DESC',
									);
								$loop_new = new WP_Query( $args_new );
								if ( $loop_new->have_posts() ) {
									while ( $loop_new->have_posts() ) : $loop_new->the_post(); ?>
										<div class="col-lg-3 product-card">
                                        <?php
                                        show_sale_percentage_loop()
                                        ?>
                                        <a href="<?php echo get_permalink( $loop_new->post->ID ) ?>" title="<?php echo esc_attr($loop_new->post->post_title ? $loop_new->post->post_title : $loop_new->post->ID); ?>">
                                            <?php if (has_post_thumbnail( $loop_new->post->ID )) {

                                                $thumbnail_id = get_post_thumbnail_id($recent["ID"]);
                                                $thumbnail_alt = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true );

                                                echo '<img class="" src="' .get_the_post_thumbnail_url($loop->post->ID, 'medium_large') . '" alt="' . $thumbnail_alt . 
                                                    '"/>'; 
                                                } else {

                                                    echo '<img class="" src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />';
                                                }  ?></a>
											<a href="<?php echo get_permalink( $loop_new->post->ID ) ?>" title="<?php echo esc_attr($loop_new->post->post_title ? $loop_new->post->post_title : $loop_new->post->ID); ?>"><h4 class="product-title"><?php echo the_title(); ?></h4></a>
											<p class="product-code"><?php the_field('product_code', $loop_new->post->ID );?>Ide jön majd a kód ha lesz</p>
											<p class="product-price"><?php echo $product->get_price_html(); ?></p>
											<p class="add-to-cart"><?php woocommerce_template_loop_add_to_cart(); ?></p>
										</div>
									<?php endwhile;
								} else {
									echo __( 'No products found' );
								}
								wp_reset_postdata();
							?>
						</div><!--/.products-->
					</div>
				</div>
			</section>

			<section class="section-separator-banner feel-free">

			</section>

			<section id="sale-products">
				<div class="container">
					<h2 class="section-title">Akciós</h2>
					<!-- WooCommerce On-Sale Products -->
					<div class="row products">
					    <?php
					        $args_sale = array(
					            'post_type'      => 'product',
					            'posts_per_page' => 4,
					            'meta_query'     => array(
					                    'relation' => 'OR',
					                    array( // Simple products type
					                        'key'           => '_sale_price',
					                        'value'         => 0,
					                        'compare'       => '>',
					                        'type'          => 'numeric'
					                    ),
					                    array( // Variable products type
					                        'key'           => '_min_variation_sale_price',
					                        'value'         => 0,
					                        'compare'       => '>',
					                        'type'          => 'numeric'
					                    )
					                )
					        );
					        $loop_sale = new WP_Query( $args_sale );
					        if ( $loop_sale->have_posts() ) {
					            while ( $loop_sale->have_posts() ) : $loop_sale->the_post(); ?>
										<div class="col-lg-3 product-card">
                                        <?php
                                        show_sale_percentage_loop()
                                        ?>
                                        <a href="<?php echo get_permalink( $loop_sale->post->ID ) ?>" title="<?php echo esc_attr($loop_sale->post->post_title ? $loop_sale->post->post_title : $loop_sale->post->ID); ?>">
                                            <?php if (has_post_thumbnail( $loop_sale->post->ID )) {

                                                $thumbnail_id = get_post_thumbnail_id($recent["ID"]);
                                                $thumbnail_alt = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true );

                                                echo '<img class="" src="' .get_the_post_thumbnail_url($loop_sale->post->ID, 'medium_large') . '" alt="' . $thumbnail_alt . 
                                                    '"/>'; 
                                                } else {

                                                    echo '<img class="" src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />';
                                                }  ?></a>
											<a href="<?php echo get_permalink( $loop_sale->post->ID ) ?>" title="<?php echo esc_attr($loop_sale->post->post_title ? $loop_sale->post->post_title : $loop_sale->post->ID); ?>"><h4 class="product-title"><?php echo the_title(); ?></h4></a>
											<p class="product-code"><?php the_field('product_code', $loop_sale->post->ID );?>Ide jön majd a kód ha lesz</p>
											<p class="product-price"><?php echo $product->get_price_html(); ?></p>
											<p class="add-to-cart"><?php woocommerce_template_loop_add_to_cart(); ?></p>
										</div>
					          <?php  endwhile;
					        } else {
					            echo __( 'No products found' );
					        }
					        wp_reset_postdata();
					    ?>
					</div>
					<!-- WooCommerce On-Sale Products -->
				</div>
			</section>

			<section class="section-separator-banner unique-production">

			</section>

			<section id="more-products">
				<div class="container">
					<h2 class="section-title">További termékeink</h2>
					<div class="row products">
						<?php
							$args_more = array(
								'post_type' => 'product',
								'posts_per_page' => 16,

								);
							$loop_more = new WP_Query( $args_more  );
							if ( $loop_more->have_posts() ) {
								while ( $loop_more->have_posts() ) : $loop_more->the_post(); ?>
										<div class="col-lg-3 product-card">
                                        <?php
                                        show_sale_percentage_loop()
                                        ?>
                                        <a href="<?php echo get_permalink( $loop_more->post->ID ) ?>" title="<?php echo esc_attr($loop_more->post->post_title ? $loop_more->post->post_title : $loop_more->post->ID); ?>">
                                            <?php if (has_post_thumbnail( $loop_sale->post->ID )) {

                                                $thumbnail_id = get_post_thumbnail_id($recent["ID"]);
                                                $thumbnail_alt = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true );

                                                echo '<img class="" src="' .get_the_post_thumbnail_url($loop_more->post->ID, 'medium_large') . '" alt="' . $thumbnail_alt . 
                                                    '"/>'; 
                                                } else {

                                                    echo '<img class="" src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />';
                                                }  ?></a>
											<a href="<?php echo get_permalink( $loop_more->post->ID ) ?>" title="<?php echo esc_attr($loop_more->post->post_title ? $loop_more->post->post_title : $loop_more->post->ID); ?>"><h4 class="product-title"><?php echo the_title(); ?></h4></a>
											<p class="product-code"><?php the_field('product_code', $loop_more->post->ID );?>Ide jön majd a kód ha lesz</p>
											<p class="product-price"><?php echo $product->get_price_html(); ?></p>
											<p class="add-to-cart"><?php woocommerce_template_loop_add_to_cart(); ?></p>
										</div>
							<?php	endwhile;
							} else {
								echo __( 'No products found' );
							}
							wp_reset_postdata();
						?>
					</div><!--/.products-->
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="col-full">
<?php
get_footer();
