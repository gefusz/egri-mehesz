<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<div class="row">
	<div class="col-lg-3 product-list-page-sidebar">
		<div class="category-list sidebar-box">
		<h3>Termékkategóriák</h3>
		<ul>
			<?php 
			$terms = get_terms( 'product_cat' ); 
			foreach( $terms as $term ) 
			{ ?>
			<li><a href="<?php echo site_url(); ?>/termekkategoria/<?php echo $term->slug; ?>"><?php echo $term->name;?></a>(<?php echo $term->count; ?>)</li>
			<?php } ?>
		 </ul>
		</div>
		<div class="sale-products sidebar-box">
			 <?php
		        $args_sale = array(
		            'post_type'      => 'product',
		            'posts_per_page' => 4,
		            'meta_query'     => array(
		                    'relation' => 'OR',
		                    array( // Simple products type
		                        'key'           => '_sale_price',
		                        'value'         => 0,
		                        'compare'       => '>',
		                        'type'          => 'numeric'
		                    ),
		                    array( // Variable products type
		                        'key'           => '_min_variation_sale_price',
		                        'value'         => 0,
		                        'compare'       => '>',
		                        'type'          => 'numeric'
		                    )
		                )
		        );
		        $loop_sale = new WP_Query( $args_sale ); ?>
		        <h3>Akciós Termékek</h3>
		        <ul>
		        <?php 	
		        if ( $loop_sale->have_posts() ) {
		            while ( $loop_sale->have_posts() ) : $loop_sale->the_post(); ?>
						<a href="<?php echo get_permalink( $loop_sale->post->ID ) ?>" title="<?php echo esc_attr($loop_sale->post->post_title ? $loop_sale->post->post_title : $loop_sale->post->ID); ?>"><li class="product-title"><?php echo the_title(); ?></li></a>
							
		          <?php  endwhile;
		        } else {
		            echo __( 'No products found' );
		        }
		        wp_reset_postdata();
		    ?>
		    </ul>
		</div>
		<div class="new-products sidebar-box">
			 <?php
		        $args_new = array(
					'post_type' => 'product',
					'posts_per_page' => 4,
			    	'orderby' => 'date',
					'order' => 'DESC',
		        );
		        $loop_new = new WP_Query( $args_new ); ?>
		        <h3>ÚJ Termékek</h3>
		        <ul>
		        <?php 	
		        if ( $loop_new->have_posts() ) {
		            while ( $loop_new->have_posts() ) : $loop_new->the_post(); ?>
						<a href="<?php echo get_permalink( $loop_new->post->ID ) ?>" title="<?php echo esc_attr($loop_new->post->post_title ? $loop_new->post->post_title : $loop_new->post->ID); ?>"><li class="product-title"><?php echo the_title(); ?></li></a>
							
		          <?php  endwhile;
		        } else {
		            echo __( 'No products found' );
		        }
		        wp_reset_postdata();
		    ?>
		    </ul>
		</div>
	</div>
	<div class="col-lg-9">
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
// do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
//do_action( 'woocommerce_sidebar' );
?> 
		</div>
	</div>
</div>
<section class="section-separator-banner unique-production">

</section>
<section class="section-separator-banner honey-quote">

</section>
<?php
get_footer( 'shop' );
