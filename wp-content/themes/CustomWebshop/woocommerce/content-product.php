<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

?>
<div class="col-lg-4 product-card">
<?php
	show_sale_percentage_loop()
	?>
	<a href="<?php echo get_permalink( $loop_new->post->ID ) ?>" title="<?php echo esc_attr($loop_new->post->post_title ? $loop_new->post->post_title : $loop_new->post->ID); ?>">
	    <?php if (has_post_thumbnail( $loop_new->post->ID )) {

	        $thumbnail_id = get_post_thumbnail_id($recent["ID"]);
	        $thumbnail_alt = get_post_meta($thumbnail_id , '_wp_attachment_image_alt', true );

	        echo '<img class="" src="' .get_the_post_thumbnail_url($loop->post->ID, 'medium_large') . '" alt="' . $thumbnail_alt . 
	            '"/>'; 
	        } else {

	            echo '<img class="" src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />';
	        }  ?></a>
		<a href="<?php echo get_permalink( $loop_new->post->ID ) ?>" title="<?php echo esc_attr($loop_new->post->post_title ? $loop_new->post->post_title : $loop_new->post->ID); ?>"><h4 class="product-title"><?php echo the_title(); ?></h4></a>
		<p class="product-code"><?php the_field('product_code', $loop_new->post->ID );?>Ide jön majd a kód ha lesz</p>
		<p class="product-price"><?php echo $product->get_price_html(); ?></p>
		<p class="add-to-cart"><?php woocommerce_template_loop_add_to_cart(); ?></p>
<!-- </li> -->
</div>
