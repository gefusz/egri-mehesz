=== WPC Added To Cart Notification for WooCommerce ===
Contributors: wpclever
Donate link: https://wpclever.net
Tags: woocommerce, woo, smart, popup, notification, add to cart, wpc
Requires at least: 4.0
Tested up to: 5.3.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WPC Added To Cart Notification will open a popup to notify the customer immediately after adding a product to cart.

== Description ==

**WPC Added to Cart Notification** is a useful plugin for completing a smooth shopping flow on your online store. It allows users to customize the notification display when a product is added to the cart from product page or shop page. This tool boost the user experience for your site and help customers acknowledge of what’s happened and guide them on what to do next.

= Live demo =

Visit our [live demo](https://demo.wpclever.net/wooac/ "live demo") here to see how this plugin works.

= Features =

- Customize the popup effect: fade, 3D unfold, zoom in, zoom out, newspaper, move horizontal, move from top, slide bottom
- Enable/ disable the image display
- Enable/ disable “View Cart” button
- Enable/disable “Continue Shopping” button
- Edit the “Continue Shopping” destination link
- Enable/disable linked product name
- Set the timer for auto-closing the notification
- Compatible with all WPClever plugins, most WooCommerce add-ons and WordPress themes
- WPML compatible for building multilingual sites
- Premium: Add a timer to auto-close the notification
- Premium: Plugin customization to match site’s design
- Premium: Lifetime updates and dedicated support

= Easy to Use =
This plugin is truly easy to use as users would know where to start right after installing this on their site. It not only display a notification that tell buyers something is added to the cart, but it also shows a hint about where to go next with a customizable View Cart & Continue Shopping buttons. Moreover, the multitude of popup effects available for users to utilize can enhance the shopping experience to a great extent and make things flow fluently on your site.

= Complete the Shopping Flow =
The biggest functionality of this WPC Added to Cart Notification plugin is to complete the Shopping Flow for your buyers. It in fact encourages your customers to browse for more products upon knowing that all items they’re considering have been properly added to the cart. The display of an Added to Cart notification will mean more than just a notice especially in sales seasons when the “holding/grasping” of a favorite/widely-sought-after  item is really a competing task. Letting your buyers know that they have successfully grasped a hot item will urge them to check out faster before the item is gone to someone else.

= Premium features: Auto-close Timer & Customization Service =
If you’re concerned that the display of notification would interfere with your customers’ purchasing process then this Auto-close timer is the solution to this issue. Premium users of this plugin are able to set the timer to automatically close this notification so that clients won’t encounter any inconvenience during their site navigation. We also provide Premium users with a free customization service in which they can request for a simple adjustment of this notification plugin to match with their site’s design scheme.

= Translators =

Available Languages: English (Default)

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress") to [us](https://wpclever.net/contact?utm_source=pot&utm_medium=wooac&utm_campaign=wporg "WPClever.net") so we can bundle it into WPC Added To Cart Notification.

= Need support? =

Visit [plugin documentation website](https://wpclever.net?utm_source=doc&utm_medium=wooac&utm_campaign=wporg "plugin documentation").

== Installation ==

1. Please make sure that you installed WooCommerce
2. Go to plugins in your dashboard and select "Add New"
3. Search for "WPC Added To Cart Notification", Install & Activate it
4. Go to settings page to choose the effect as you want

== Changelog ==

= 1.2.2 =
* Updated: Compatible with WordPress 5.3.2 & WooCommerce 3.9.2

= 1.2.1 =
* Updated: Optimized the code

= 1.2.0 =
* Updated: Compatible with WordPress 5.3 & WooCommerce 3.8.x

= 1.1.9 =
* Updated: Compatible with WooCommerce 3.7.x

= 1.1.8 =
* Updated: Optimized the code

= 1.1.7 =
* Updated: Compatible with WooCommerce 3.6.x
* Added: Auto close (Premium Version)

= 1.1.6 =
* Fixed: Showing wrong product

= 1.1.5 =
* Updated: Compatible with WooCommerce 3.5.7
* Updated: Optimized the code

= 1.1.4 =
* Updated: Optimized the code

= 1.1.3 =
* Added: Custom URL for continue shopping button
* Updated: Optimized the code

= 1.1.2 =
* Updated: Compatible with WooCommerce 3.5.1
* Updated: Optimized the code

= 1.1.1 =
* Updated: Compatible with WooCommerce 3.5.0

= 1.1.0 =
* Added: Show the popup when adding product to cart on the single product page

= 1.0.5 =
* Fixed: Error when WooCommerce is not active

= 1.0.4 =
* Updated: Settings page style

= 1.0.3 =
* Updated: Compatible with WooCommerce 3.3.5

= 1.0.2 =
* Updated: Compatible with WooCommerce 3.3.3

= 1.0.1 =
* Fixed: Minor CSS issue

= 1.0 =
* Released