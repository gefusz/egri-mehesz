<?php
/*
Plugin Name: WPC Added To Cart Notification for WooCommerce
Plugin URI: https://wpclever.net/
Description: WPC Added To Cart Notification will open a popup to notify the customer immediately after adding a product to cart.
Version: 1.2.2
Author: WPClever.net
Author URI: https://wpclever.net
Text Domain: wooac
Domain Path: /languages/
Requires at least: 4.0
Tested up to: 5.3.2
WC requires at least: 3.0
WC tested up to: 3.9.2
*/

defined( 'ABSPATH' ) || exit;

! defined( 'WOOAC_VERSION' ) && define( 'WOOAC_VERSION', '1.2.2' );
! defined( 'WOOAC_URI' ) && define( 'WOOAC_URI', plugin_dir_url( __FILE__ ) );
! defined( 'WOOAC_REVIEWS' ) && define( 'WOOAC_REVIEWS', 'https://wordpress.org/support/plugin/woo-added-to-cart-notification/reviews/?filter=5' );
! defined( 'WOOAC_CHANGELOG' ) && define( 'WOOAC_CHANGELOG', 'https://wordpress.org/plugins/woo-added-to-cart-notification/#developers' );
! defined( 'WOOAC_DISCUSSION' ) && define( 'WOOAC_DISCUSSION', 'https://wordpress.org/support/plugin/woo-added-to-cart-notification' );
! defined( 'WPC_URI' ) && define( 'WPC_URI', WOOAC_URI );

include 'includes/wpc-menu.php';
include 'includes/wpc-dashboard.php';

if ( ! function_exists( 'wooac_init' ) ) {
	add_action( 'plugins_loaded', 'wooac_init', 11 );

	function wooac_init() {
		// load text-domain
		load_plugin_textdomain( 'wooac', false, basename( __DIR__ ) . '/languages/' );

		if ( ! function_exists( 'WC' ) || ! version_compare( WC()->version, '3.0.0', '>=' ) ) {
			add_action( 'admin_notices', 'wooac_notice_wc' );

			return;
		}

		if ( ! class_exists( 'WPCleverWooac' ) ) {
			class WPCleverWooac {
				function __construct() {
					// menu
					add_action( 'admin_menu', array( $this, 'wooac_admin_menu' ) );

					// frontend scripts
					add_action( 'wp_enqueue_scripts', array( $this, 'wooac_wp_enqueue_scripts' ) );

					// link
					add_filter( 'plugin_action_links', array( $this, 'wooac_action_links' ), 10, 2 );
					add_filter( 'plugin_row_meta', array( $this, 'wooac_row_meta' ), 10, 2 );

					// add the time
					add_action( 'woocommerce_add_to_cart', array( $this, 'wooac_add_to_cart' ), 10 );

					// fragments
					add_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'wooac_cart_fragment' ) );

					// footer
					add_action( 'wp_footer', array( $this, 'wooac_wp_footer' ) );
				}

				function wooac_admin_menu() {
					add_submenu_page( 'wpclever', esc_html__( 'WPC Added To Cart Notification', 'wooac' ), esc_html__( 'Added To Cart Notification', 'wooac' ), 'manage_options', 'wpclever-wooac', array(
						&$this,
						'wooac_admin_menu_content'
					) );
				}

				function wooac_admin_menu_content() {
					$active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'settings';
					?>
                    <div class="wpclever_settings_page wrap">
                        <h1 class="wpclever_settings_page_title"><?php echo esc_html__( 'WPC Added To Cart Notification', 'wooac' ) . ' ' . WOOAC_VERSION; ?></h1>
                        <div class="wpclever_settings_page_desc about-text">
                            <p>
								<?php printf( esc_html__( 'Thank you for using our plugin! If you are satisfied, please reward it a full five-star %s rating.', 'wooac' ), '<span style="color:#ffb900">&#9733;&#9733;&#9733;&#9733;&#9733;</span>' ); ?>
                                <br/>
                                <a href="<?php echo esc_url( WOOAC_REVIEWS ); ?>"
                                   target="_blank"><?php esc_html_e( 'Reviews', 'wooac' ); ?></a> | <a
                                        href="<?php echo esc_url( WOOAC_CHANGELOG ); ?>"
                                        target="_blank"><?php esc_html_e( 'Changelog', 'wooac' ); ?></a>
                                | <a href="<?php echo esc_url( WOOAC_DISCUSSION ); ?>"
                                     target="_blank"><?php esc_html_e( 'Discussion', 'wooac' ); ?></a>
                            </p>
                        </div>
                        <div class="wpclever_settings_page_nav">
                            <h2 class="nav-tab-wrapper">
                                <a href="<?php echo admin_url( 'admin.php?page=wpclever-wooac&tab=settings' ); ?>"
                                   class="<?php echo $active_tab === 'settings' ? 'nav-tab nav-tab-active' : 'nav-tab'; ?>">
									<?php esc_html_e( 'Settings', 'wooac' ); ?>
                                </a>
                                <a href="<?php echo admin_url( 'admin.php?page=wpclever-wooac&tab=premium' ); ?>"
                                   class="<?php echo $active_tab === 'premium' ? 'nav-tab nav-tab-active' : 'nav-tab'; ?>">
									<?php esc_html_e( 'Premium Version', 'wooac' ); ?>
                                </a>
                            </h2>
                        </div>
                        <div class="wpclever_settings_page_content">
							<?php if ( $active_tab === 'settings' ) { ?>
                                <form method="post" action="options.php">
									<?php wp_nonce_field( 'update-options' ) ?>
                                    <table class="form-table">
                                        <tr class="heading">
                                            <th colspan="2">
												<?php esc_html_e( 'General', 'wooac' ); ?>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Popup effect', 'wooac' ); ?></th>
                                            <td>
                                                <select name="wooac_effect">
                                                    <option
                                                            value="mfp-fade" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-fade' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Fade', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-zoom-in" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-zoom-in' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Zoom in', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-zoom-out" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-zoom-out' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Zoom out', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-newspaper" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-newspaper' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Newspaper', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-move-horizontal" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-move-horizontal' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Move horizontal', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-move-from-top" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-move-from-top' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Move from top', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-3d-unfold" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-3d-unfold' ? 'selected' : '' ); ?>>
														<?php esc_html_e( '3d unfold', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="mfp-slide-bottom" <?php echo( get_option( 'wooac_effect', 'mfp-3d-unfold' ) === 'mfp-slide-bottom' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Slide bottom', 'wooac' ); ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Show image', 'wooac' ); ?></th>
                                            <td>
                                                <select name="wooac_show_image">
                                                    <option
                                                            value="yes" <?php echo( get_option( 'wooac_show_image', 'yes' ) === 'yes' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Yes', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="no" <?php echo( get_option( 'wooac_show_image', 'yes' ) === 'no' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'No', 'wooac' ); ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Show "View Cart" button', 'wooac' ); ?></th>
                                            <td>
                                                <select name="wooac_show_view_cart">
                                                    <option
                                                            value="yes" <?php echo( get_option( 'wooac_show_view_cart', 'yes' ) === 'yes' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Yes', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="no" <?php echo( get_option( 'wooac_show_view_cart', 'yes' ) === 'no' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'No', 'wooac' ); ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Show "Continue Shopping" button', 'wooac' ); ?></th>
                                            <td>
                                                <select name="wooac_show_continue_shopping">
                                                    <option
                                                            value="yes" <?php echo( get_option( 'wooac_show_continue_shopping', 'yes' ) === 'yes' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Yes', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="no" <?php echo( get_option( 'wooac_show_continue_shopping', 'yes' ) === 'no' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'No', 'wooac' ); ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Continue shopping link', 'wooac' ); ?></th>
                                            <td>
                                                <input type="url" name="wooac_continue_url"
                                                       value="<?php echo get_option( 'wooac_continue_url' ); ?>"
                                                       class="regular-text code"/> <span
                                                        class="description">
											<?php esc_html_e( 'By default, only hide the popup when clicking on "Continue Shopping" button.', 'wooac' ); ?>
										</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Add link to product name', 'wooac' ); ?></th>
                                            <td>
                                                <select name="wooac_add_link">
                                                    <option
                                                            value="yes" <?php echo( get_option( 'wooac_add_link', 'yes' ) === 'yes' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'Yes', 'wooac' ); ?>
                                                    </option>
                                                    <option
                                                            value="no" <?php echo( get_option( 'wooac_add_link', 'yes' ) === 'no' ? 'selected' : '' ); ?>>
														<?php esc_html_e( 'No', 'wooac' ); ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php esc_html_e( 'Auto close', 'wooac' ); ?></th>
                                            <td>
                                                <input name="wooac_auto_close" type="number" min="0" max="300000"
                                                       step="1"
                                                       value="<?php echo get_option( 'wooac_auto_close', '2000' ); ?>"/>ms.
                                                <span class="description">
											<?php esc_html_e( 'Set the time is zero to disable auto close.', 'wooac' ); ?>
										</span>
                                                <p style="color: red">
                                                    This feature is only available on the Premium Version. Click <a
                                                            href="https://wpclever.net/downloads/woocommerce-added-to-cart-notification?utm_source=pro&utm_medium=wooac&utm_campaign=wporg"
                                                            target="_blank">here</a> to buy, just $19.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr class="submit">
                                            <th colspan="2">
                                                <input type="submit" name="submit" class="button button-primary"
                                                       value="<?php esc_html_e( 'Update Options', 'wooac' ); ?>"/>
                                                <input type="hidden" name="action" value="update"/>
                                                <input type="hidden" name="page_options"
                                                       value="wooac_effect,wooac_show_image,wooac_show_view_cart,wooac_show_continue_shopping,wooac_continue_url,wooac_add_link,wooac_auto_close"/>
                                            </th>
                                        </tr>
                                    </table>
                                </form>
							<?php } elseif ( $active_tab === 'premium' ) { ?>
                                <div class="wpclever_settings_page_content_text">
                                    <p>
                                        Get the Premium Version just $19! <a
                                                href="https://wpclever.net/downloads/woocommerce-added-to-cart-notification?utm_source=pro&utm_medium=wooac&utm_campaign=wporg"
                                                target="_blank">https://wpclever.net/downloads/woocommerce-added-to-cart-notification</a>
                                    </p>
                                    <p><strong>Extra features for Premium Version:</strong></p>
                                    <ul style="margin-bottom: 0">
                                        <li>- Add the time to auto close popup.</li>
                                        <li>- Get the lifetime update & premium support.</li>
                                    </ul>
                                </div>
							<?php } ?>
                        </div>
                    </div>
					<?php
				}

				function wooac_wp_enqueue_scripts() {
					// feather icons
					wp_enqueue_style( 'wooac-feather', WOOAC_URI . 'assets/libs/feather/feather.css' );

					// magnific
					wp_enqueue_style( 'magnific-popup', WOOAC_URI . 'assets/libs/magnific-popup/magnific-popup.css' );
					wp_enqueue_script( 'magnific-popup', WOOAC_URI . 'assets/libs/magnific-popup/jquery.magnific-popup.min.js', array( 'jquery' ), WOOAC_VERSION, true );

					// main style & js
					wp_enqueue_style( 'wooac-frontend', WOOAC_URI . 'assets/css/frontend.css' );
					wp_enqueue_script( 'wooac-frontend', WOOAC_URI . 'assets/js/frontend.js', array( 'jquery' ), WOOAC_VERSION, true );
					wp_localize_script( 'wooac-frontend', 'wooac_vars', array(
							'ajax_url' => admin_url( 'admin-ajax.php' ),
							'effect'   => get_option( 'wooac_effect', 'mfp-3d-unfold' ),
							'close'    => (int) get_option( 'wooac_auto_close', '2000' ),
							'nonce'    => wp_create_nonce( 'wooac-nonce' )
						)
					);
				}

				function wooac_action_links( $links, $file ) {
					static $plugin;
					if ( ! isset( $plugin ) ) {
						$plugin = plugin_basename( __FILE__ );
					}
					if ( $plugin === $file ) {
						$settings_link = '<a href="' . admin_url( 'admin.php?page=wpclever-wooac&tab=settings' ) . '">' . esc_html__( 'Settings', 'wooac' ) . '</a>';
						$links[]       = '<a href="' . admin_url( 'admin.php?page=wpclever-wooac&tab=premium' ) . '">' . esc_html__( 'Premium Version', 'wooac' ) . '</a>';
						array_unshift( $links, $settings_link );
					}

					return $links;
				}

				function wooac_row_meta( $links, $file ) {
					static $plugin;
					if ( ! isset( $plugin ) ) {
						$plugin = plugin_basename( __FILE__ );
					}
					if ( $plugin === $file ) {
						$row_meta = array(
							'support' => '<a href="https://wpclever.net/support?utm_source=support&utm_medium=wooac&utm_campaign=wporg" target="_blank">' . esc_html__( 'Premium support', 'wooac' ) . '</a>',
						);

						return array_merge( $links, $row_meta );
					}

					return (array) $links;
				}

				function wooac_get_product() {
					$items       = WC()->cart->get_cart();
					$return_html = '<div class="wooac-popup mfp-with-anim">';
					if ( count( $items ) > 0 ) {
						array_multisort( array_column( $items, 'wooac_time' ), SORT_ASC, $items );
						$wooac_product = end( $items )['data'];
						if ( ! in_array( $wooac_product->get_id(), apply_filters( 'wooac_exclude_ids', array( 0 ) ), true ) ) {
							if ( get_option( 'wooac_show_image', 'yes' ) === 'yes' ) {
								$return_html .= '<div class="wooac-image">' . $wooac_product->get_image() . '</div>';
							}
							if ( get_option( 'wooac_add_link', 'yes' ) === 'yes' ) {
								$return_html .= '<div class="wooac-text">' . sprintf( esc_html__( '%s was added to the cart.', 'wooac' ), '<a href="' . $wooac_product->get_permalink() . '">' . $wooac_product->get_name() . '</a>' ) . '</div>';
							} else {
								$return_html .= '<div class="wooac-text">' . sprintf( esc_html__( '%s was added to the cart.', 'wooac' ), '<span>' . $wooac_product->get_name() . '</span>' ) . '</div>';
							}
							if ( ( get_option( 'wooac_show_view_cart', 'yes' ) === 'yes' ) || ( get_option( 'wooac_show_continue_shopping', 'yes' ) === 'yes' ) ) {
								$return_html .= '<div class="wooac-action">';
								if ( get_option( 'wooac_show_view_cart', 'yes' ) === 'yes' ) {
									$return_html .= '<a id="wooac-cart" href="' . wc_get_cart_url() . '">' . esc_html__( 'View Cart', 'wooac' ) . '</a>';
								}
								if ( get_option( 'wooac_show_continue_shopping', 'yes' ) === 'yes' ) {
									$return_html .= '<a id="wooac-continue" href="#" data-url="' . get_option( 'wooac_continue_url' ) . '">' . esc_html__( 'Continue Shopping', 'wooac' ) . '</a>';
								}
								$return_html .= '</div>';
							}
						}
					}
					$return_html .= '</div>';

					return $return_html;
				}

				function wooac_add_to_cart( $cart_item_key ) {
					if ( isset( WC()->cart->cart_contents[ $cart_item_key ]['woosb_parent_id'] ) || isset( WC()->cart->cart_contents[ $cart_item_key ]['wooco_parent_id'] ) ) {
						// prevent bundled products and composite products
						WC()->cart->cart_contents[ $cart_item_key ]['wooac_time'] = time() - 10000;
					} else {
						WC()->cart->cart_contents[ $cart_item_key ]['wooac_time'] = time();
					}
				}

				function wooac_cart_fragment( $fragments ) {
					ob_start();
					echo $this->wooac_get_product();
					$fragments['.wooac-popup'] = ob_get_clean();

					return $fragments;
				}

				function wooac_wp_footer() {
					echo '<div class="wooac-popup mfp-with-anim"></div>';
					if ( isset( $_POST['add-to-cart'] ) || isset( $_GET['add-to-cart'] ) ) {
						?>
                        <script>
                          jQuery(document).ready(function() {
                            jQuery('body').on('wc_fragments_refreshed', function() {
                              wooac_show();
                            });
                          });
                        </script>
						<?php
					}
				}
			}

			new WPCleverWooac();
		}
	}
} else {
	add_action( 'admin_notices', 'wooac_notice_premium' );
}

if ( ! function_exists( 'wooac_notice_wc' ) ) {
	function wooac_notice_wc() {
		?>
        <div class="error">
            <p><strong>WPC Added To Cart Notification</strong> requires WooCommerce version 3.0.0 or greater.</p>
        </div>
		<?php
	}
}

if ( ! function_exists( 'wooac_notice_premium' ) ) {
	function wooac_notice_premium() {
		?>
        <div class="error">
            <p>Seems you're using both free and premium version of <strong>WPC Added To Cart Notification</strong>.
                Please deactivate the free version when using the premium version.</p>
        </div>
		<?php
	}
}